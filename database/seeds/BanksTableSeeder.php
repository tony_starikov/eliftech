<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            [
                'name' => 'The first bank',
                'rate' => 1,
                'loan' => 10000,
                'payment' => 5,
                'term' => 12,
            ],

            [
                'name' => 'The second bank',
                'rate' => 2,
                'loan' => 20000,
                'payment' => 10,
                'term' => 24,
            ],

            [
                'name' => 'The third bank',
                'rate' => 3,
                'loan' => 30000,
                'payment' => 15,
                'term' => 36,
            ],
        ]);
    }
}
