<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bank_id')->nullable();
            $table->string('bank_name')->nullable();
            $table->integer('loan')->nullable();
            $table->integer('first_payment')->nullable();
            $table->integer('monthly_payment')->nullable();
            $table->integer('rate')->nullable();
            $table->integer('term')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculations');
    }
}
