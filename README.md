## About

Test project for eliftech. Web application where you can add banks and calculate mortgage payments.

## Instalation

Copy your project to the server.

Make a link from server/public/project_name to laravel/public folder.

Run composer install.

Edit .env file.

Create database file in laravel_folder/database/database.sqlite if you are using sqlite db.

Run php artisan key:generate.

Run php artisan migrate --seed.

Enjoy)