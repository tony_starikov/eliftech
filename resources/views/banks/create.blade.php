@extends('master')

@section('main')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <h2 class="display-6 text-center mb-4">Add new bank</h2>

    <div class="row row-cols-1 mb-3 text-center">
        <div class="col d-flex justify-content-center">
            <div class="card mb-4 rounded-3 shadow-sm w-75">
                <div class="card-body">
                    <form action="{{ route('bank.store') }}" method="post">
                        @csrf

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Name</span>
                            <input type="text" name="name" class="form-control" placeholder="min:5 | max:255 characters" aria-label="Username" aria-describedby="basic-addon1">
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Interest rate</span>
                            <input type="text" name="rate" class="form-control" placeholder="min:1 | max:100" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">%</span>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Maximum loan</span>
                            <input type="text" name="loan" class="form-control" placeholder="min:10 000 | max:1 000 000" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">$</span>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Minimum down payment</span>
                            <input type="text" name="payment" class="form-control" placeholder="min:1 | max:100" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">%</span>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Loan term</span>
                            <input type="text" name="term" class="form-control" placeholder="min:12 | max:120" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">months</span>
                        </div>

                        <button type="submit" class="btn btn-success">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
