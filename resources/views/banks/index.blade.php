@extends('master')

@section('main')
    <h2 class="display-6 text-center mb-4">Bank list</h2>

    <div class="text-center mb-4">
        <a href="{{ route('bank.create') }}" class="btn btn-success">Add new bank</a>
    </div>



    <div class="table-responsive">
        <table class="table text-center">
            <thead>
            <tr>
                <th style="width: 5%;">№</th>
                <th style="width: 15%;">Name</th>
                <th style="width: 15%;">Interest rate</th>
                <th style="width: 15%;">Max loan</th>
                <th style="width: 15%;">Min down payment</th>
                <th style="width: 15%;">Term</th>
                <th style="width: 20%;"></th>
            </tr>
            </thead>

            <tbody>

            @foreach($banks as $bank)
                <tr>
                    <th>{{ $bank->id }}</th>
                    <th>{{ $bank->name }}</th>
                    <td>{{ $bank->rate }}%</td>
                    <td>{{ $bank->loan }}$</td>
                    <td>{{ $bank->payment }}%</td>
                    <td>{{ $bank->term }} months</td>
                    <td>
                        <div class="btn-group" role="group">
                            <a href="{{ route('bank.show', $bank) }}" class="btn btn-primary">Calculations</a>
                            <a href="{{ route('bank.edit', $bank) }}" class="btn btn-warning">Edit</a>
                            <form action="{{ route('bank.destroy', $bank) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>

                        </div>

                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection
