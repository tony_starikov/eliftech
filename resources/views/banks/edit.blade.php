@extends('master')

@section('main')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <h2 class="display-6 text-center mb-4">Edit {{ $bank->name }}</h2>

    <div class="row row-cols-1 mb-3 text-center">
        <div class="col d-flex justify-content-center">
            <div class="card mb-4 rounded-3 shadow-sm w-75">
                <div class="card-body">
                    <form action="{{ route('bank.update', $bank) }}" method="post">
                        @method('PUT')
                        @csrf

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Name</span>
                            <input type="text" name="name" value="{{ $bank->name }}" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Interest rate</span>
                            <input type="text" name="rate" value="{{ $bank->rate }}" class="form-control" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">%</span>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Maximum loan</span>
                            <input type="text" name="loan" value="{{ $bank->loan }}" class="form-control" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">$</span>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Minimum down payment</span>
                            <input type="text" name="payment" value="{{ $bank->payment }}" class="form-control" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">%</span>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Loan term</span>
                            <input type="text" name="term" value="{{ $bank->term }}" class="form-control" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">months</span>
                        </div>

                        <button type="submit" class="btn btn-warning">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
