@extends('master')

@section('main')
    <h2 class="display-6 text-center my-4">This is a web application where you can add banks and calculate mortgage payments for free.</h2>
    <h3 class="text-center mb-4">Determine what you could pay each month by using this mortgage calculator to calculate estimated monthly payments and rate options for a variety of loan terms.</h3>
@endsection
