@extends('master')

@section('main')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <h2 class="display-6 text-center mb-4">Monthly payment calculator</h2>

    <div class="row row-cols-1 mb-3 text-center">
        <div class="col d-flex justify-content-center">
            <div class="card mb-4 rounded-3 shadow-sm w-75">
                <div class="card-body">
                    <form action="{{ route('calculation.store') }}" method="post">
                        @csrf

                        <div class="form-floating mb-3">
                            <select name="bank_id" class="form-select" id="floatingSelect" aria-label="Floating label select example">
                                @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">
                                        {{ $bank->name }} | Interest rate: {{ $bank->rate }}% | Max loan: {{ $bank->loan }}$ | Minimum down payment: {{ $bank->payment }}% | Loan term: {{ $bank->term }} months
                                    </option>
                                @endforeach
                            </select>
                            <label for="floatingSelect">Choose a bank</label>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">Initial loan</span>
                            <input type="text" name="loan" class="form-control" placeholder="200000" aria-label="Username" aria-describedby="basic-addon1">
                            <span class="input-group-text">$</span>
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon2">Down payment</span>
                            <input type="text" name="first_payment" class="form-control" placeholder="20000" aria-label="Username" aria-describedby="basic-addon2">
                            <span class="input-group-text">$</span>
                        </div>

                        <button type="submit" class="btn btn-primary">Calculate</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
