@extends('master')

@section('main')
    <h2 class="display-6 text-center mb-4">Calculation history</h2>

    <div class="text-center mb-4">
        <a href="{{ route('calculation.create') }}" class="btn btn-success">Calculator</a>
    </div>



    <div class="table-responsive">
        <table class="table text-center">
            <thead>
            <tr>
                <th style="width: 5%;">№</th>
                <th style="width: 15%;">Bank</th>
                <th style="width: 15%;">Loan</th>
                <th style="width: 15%;">Down payment</th>
                <th style="width: 15%;">Monthly payment</th>
                <th style="width: 15%;">Rate</th>
                <th style="width: 10%;">Term</th>
                <th style="width: 10%;"></th>
            </tr>
            </thead>

            <tbody>

            @if($calculations != null)
                @foreach($calculations as $calculation)
                    <tr>
                        <th>{{ $calculation->id }}</th>
                        <th>{{ $calculation->bank_name }}</th>
                        <td>{{ $calculation->loan }}$</td>
                        <td>{{ $calculation->first_payment }}$</td>
                        <td>{{ $calculation->monthly_payment }}$</td>
                        <td>{{ $calculation->rate }}%</td>
                        <td>{{ $calculation->term }} months</td>
                        <td>
                            <div class="btn-group" role="group">
                                <form action="{{ route('calculation.destroy', $calculation) }}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>

                            </div>

                        </td>
                    </tr>
                @endforeach
            @endif



            </tbody>
        </table>
    </div>
@endsection
