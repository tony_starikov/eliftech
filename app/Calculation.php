<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calculation extends Model
{
    protected $fillable = [
        'bank_id',
        'bank_name',
        'loan',
        'first_payment',
        'monthly_payment',
        'term',
        'rate',
    ];
}
