<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = [
        'name',
        'rate',
        'loan',
        'payment',
        'term',
    ];

    public function calculations()
    {
        return $this->hasMany(Calculation::class);
    }
}
