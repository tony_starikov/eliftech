<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Calculation;
use Illuminate\Http\Request;

class CalculationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $calculations = Calculation::orderBy('id', 'desc')->get();
        return view('calculations.index', compact('calculations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks = Bank::all();
        return view('calculations.create', compact('banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bank = Bank::where('id', $request->bank_id)->first();

        $minFirstPayment = ($request->loan / 100) * $bank->payment;

        $request->validate([
            'loan' => 'required|integer|max:'. $bank->loan,
            'first_payment' => 'required|integer|min:' . $minFirstPayment,
        ]);

        $parameters = $request->all();
        $parameters['bank_name'] = $bank->name;
        $parameters['term'] = $bank->term;
        $parameters['rate'] = $bank->rate;

        $r = $bank->rate;
        $P = $request->loan;
        $n = $bank->term;

        $formula1 = $r / 12;
        $formula2 = (1 + $formula1) ** $n;
        $formula3 = $formula2 - 1;

        $monthlyPayment = ($P * $formula1 * $formula2) / $formula3;

        $parameters['monthly_payment'] = (int)$monthlyPayment;

        Calculation::create($parameters);

        return redirect()->route('calculation.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calculation  $calculation
     * @return \Illuminate\Http\Response
     */
    public function show(Calculation $calculation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calculation  $calculation
     * @return \Illuminate\Http\Response
     */
    public function edit(Calculation $calculation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calculation  $calculation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Calculation $calculation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calculation  $calculation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Calculation $calculation)
    {
        $calculation->delete();

        return redirect()->route('calculation.index');
    }
}
